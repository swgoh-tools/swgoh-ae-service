const fs = require('fs')
const expressServer = require('src/expressServer');
const server = expressServer.listen(process.env.PORT || 3000, ()=>{
  console.log('Listening on ', server.address().port)
})
global.WORKING_DIR = process.env.WORKING_DIR || 'data'
global.TARGET_DIR = process.env.TARGET_DIR || 'data/output'
global.ASSET_VERSION = process.env.ASSET_VERSION || '2046'
process.on('unhandledRejection', error => {
  console.log(error)
});
const CreateSettings = ()=>{
  fs.mkdirSync(baseDir+'/data/output', {recursive: true})
  fs.mkdirSync(baseDir+'/'+WORKING_DIR, {recursive: true})
  fs.mkdirSync(baseDir+'/'+TARGET_DIR, {recursive: true})
  const obj = {
    workingDirectory: baseDir+'/'+WORKING_DIR,
    defaultOutputDirectory: baseDir+'/'+TARGET_DIR,
    defaultAssetVersion:  ASSET_VERSION
  }
  fs.writeFileSync(baseDir+'/asset-getter/settings.json', JSON.stringify(obj, null, 2))
}
CreateSettings()
