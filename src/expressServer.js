const express = require('express')
const fs = require('fs')
const path = require('path')
const compression = require('compression')
const { exec, spawn } = require('child_process')
const app = express()

const GetAssets = (obj)=>{
  return new Promise(resolve=>{
    try{
      let args = [];
      for(let i in obj){
        if(i == 't'){
          args.push('-t')
          args.push(baseDir+'/'+WORKING_DIR+'/'+obj[i])
        }else{
          args.push('-'+i)
          args.push(obj[i])
        }
      }
      const tempSpawn = spawn('./SwgohAssetGetterConsole', args, {
        cwd: baseDir+'/asset-getter'
      })
      tempSpawn.on('error', error =>{
        console.log(error)
      })
      tempSpawn.stdout.on('data', data =>{
        if(process.env.DEBUG) console.log(data.toString())
      })
      tempSpawn.stderr.on('data', data =>{
        console.log(data.toString())
      })
      tempSpawn.on('close', code =>{
        if(process.env.DEBUG) console.log('Process exited with code '+code)
        resolve()
      })
    }catch(e){
      console.log(e)
      resolve()
    }
  })
}
const RemoveFile = (file)=>{
  try{
    fs.unlinkSync(file)
  }catch(e){
    console.log('Error removing '+file)
  }
}
const ReadFile = (file)=>{
  try{
    const obj = fs.readFileSync(file)
    if(obj) return JSON.parse(obj)
  }catch(e){
    console.log('Error reading '+file)
  }
}
const WriteFile = (file, data)=>{
  try{
    fs.writeFileSync(file, JSON.stringify(data, null, 2))
  }catch(e){
    console.log('Error writing '+file)
  }
}
app.use(express.json({ limit: '100MB'}))
app.use(compression());
app.get('/single', async(req, res)=>{
  if(req.query.name){
    const obj = {
      'single': req.query.name,
      't': 'temp'
    }
    if(req.query.version) obj.v = req.query.version
    let imgPrefix = ''
    const tempArray = req.query.name.split('_')
    if(tempArray.length > 1) imgPrefix = tempArray[0]
    await GetAssets(obj)
    res.sendFile(baseDir+'/'+WORKING_DIR+'/temp/'+imgPrefix+'/tex.'+req.query.name+'.png', (err)=>{
      if(err){
        res.status(404).send(err).end()
        console.log(err)
      }else{
        RemoveFile(baseDir+'/'+WORKING_DIR+'/temp/'+imgPrefix+'/tex.'+req.query.name+'.png')
        res.end()
      }
    })
  }else{
    res.end()
  }
})
app.post('/data', async(req, res)=>{
  if(req.body.opt){
    await GetAssets(req.body.opt)
    if(process.env.DEBUG) console.log('done')
  }
  res.json(1).end()
})
app.get('/list', async(req, res)=>{
  let listObj
  if(req.query.version){
    listObj = await ReadFile(baseDir+'/'+WORKING_DIR+'/list/'+req.query.version+'/AssetNames.json')
    if(!listObj){
      const obj = {
        getAssetNames: 0,
        v: req.query.version
      }
      await GetAssets(obj)
      listObj = await ReadFile(baseDir+'/dataAssetNames.json')
      if(listObj){
        fs.mkdirSync(baseDir+'/'+WORKING_DIR+'/list/'+req.query.version, {recursive: true})
        WriteFile(baseDir+'/'+WORKING_DIR+'/list/'+req.query.version+'/AssetNames.json', listObj)
      }
    }
  }
  if(listObj){
    res.json(listObj).end()
  }else{
    res.status(404).end()
  }
})
module.exports = app;
