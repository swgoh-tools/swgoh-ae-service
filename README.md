# DEPRECRATED

As of 2022/02 with swgoh's unity upgrade, this version of the asset extractor no longer functions.  The replacement is the [swgoh Asset Extractor 2](https://gitlab.com/swgoh-tools/swgoh-ae2) project which provides the same or more functionality.

# swgoh-ae-service
Uses https://gitlab.com/swgoh-tools/swgoh-ae ubuntu 20.04 LTS built runtime with expressjs to request asset files.

# environment variables
- ASSET_VERSION - the default asset version. Defaults to `2046`
- WORKING_DIR - working directory relative to the base directory. For the docker image the base directory is `/home/node/app`. a `WORKING_DIR` of `data` will make working directory `/home/node/app/data`. Defaults to `data`
- TARGET_DIR - default directory for asset output relative to the base directory. For the docker image the base directory is `/home/node/app`. a `TARGET_DIR` of `data/output` will make working directory `/home/node/app/data/output`. Defaults to `data/output`
- PORT - port for the express server. defaults to `3000`
- DEBUG - if set to `true` will show the output of the asset getter.

# building with docker
```sh
docker build -t swgoh-ae-service .
docker run --name swgoh-ae-service\
  -d \
  --restart always \
  -p 3000:3000 \
  -v $(pwd)/data:/home/node/app/data \
  --env-file .env-swgoh-aw-service \
  swgoh-ae-service
```
#usage
- /single?name=charui_b1&version=2046 - `GET` request the asset for '`name` parameter. `version` is requeste asset version, defaults to `ASSET_VERSION` above. This will only return a single file and only those images that start with `tex.` EX: `tex.charui_b1.png`. It will also remove the file after it is returned, some temp directories will remain. based on `SwgohAssetGetterConsole -single charui_b1`
- /list?version=2046 - `GET`request for all available assets for specified `version`. This will return an array with all the asset names. base on `SwgohAssetGetterConsole -getAssetNames`
- /data - `POST` request based off the usage information from https://gitlab.com/swgoh-tools/swgoh-ae . The main parameter must be specified first. the body is an obj of the flag for the key and the value for the value (if no value required like for `all` use `0` for the value)
`t` for target directory is relative to the `WORKING_DIR` from above
example body for `POST` request:
This will request a download of `all` assets for `v` 2046 to `t` directory `/home/node/app/data/full/2046`
```js
{
    "opt": {
        "all": 0,
        "v": 2046,
        "t": "full/2046"
    }
}
```
This will `downloadDiff` of assets between `v` 2046 and 2044 to `t` of `/home/node/app/data/diff/2046`
```js
{
    "opt": {
        "downloadDiff": 2044,
        "v": 2046,
        "t": "diff/2046"
    }
}
```
