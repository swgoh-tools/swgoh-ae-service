FROM node:12
RUN apt update && apt install -y libgdiplus
RUN mkdir -p /home/node/app/; mkdir /home/node/app/data; mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app && chmod -R 777 /home/node/app/data
USER node
WORKDIR /home/node/app
COPY package*.json ./
RUN npm install
COPY --chown=node:node . .
CMD node server.js
